#ifndef __GoTPARAMETRIZEDVOLUME_H_
#define __GoTPARAMETRIZEDVOLUME_H_

#include <G4SystemOfUnits.hh>
#include "G4Types.hh"

#include <map>
#include <string>

#include <GoTParametrizedObject.hh>

class G4LogicalVolume;
class G4VisAttributes;
class G4VPhysicalVolume;
class G4Step;
class G4MaterialPropertiesTable;
class TFormula;

#include <G4MaterialPropertyVector.hh>

#include <GoTParametrizedVolume.hh>
#include <libconfig.h++>
namespace icedcode
{
  const float inch = 2.54*cm;

  class GoTParametrizedVolume: public GoTParametrizedObject
  {
  protected:
    static G4VisAttributes  *fgold;
    static G4VisAttributes  *fsilver;
    static G4VisAttributes  *fbrown;
    static G4VisAttributes  *forange;
    static G4VisAttributes  *fcyan;
    static G4VisAttributes  *ftransparent;

  protected:
    struct VolumeParameters: public GoTParametrizedObject::ObjectParameters
    {
      //overlaps checking
      bool CheckOverlaps = false;

      bool IsOk = false;

      virtual void CalculateParameters ()
      {
      }

    };

  public:
    class GoTVolume: public GoTParametersMgr
    {
    public:
      GoTVolume (VolumeParameters* ptp = NULL) {fTheParameters=ptp;};
      virtual ~GoTVolume ();

      virtual G4LogicalVolume* Construct (const VolumeParameters* = NULL) =0;
      G4LogicalVolume* GetLogicalVolume ()
      {
        return fTheLogicalVolume;
      }
      const G4LogicalVolume* GetLogicalVolume () const
      {
        return fTheLogicalVolume;
      }


      G4LogicalVolume         *fTheLogicalVolume=NULL;
    };


  public:
    G4LogicalVolume* GetLogicalVolume (std::string);

  protected:
    GoTParametrizedVolume ();
    virtual ~GoTParametrizedVolume ();


  protected:
    std::map <const G4LogicalVolume*, std::string> fLogicalVolDict;
  };
}

#endif
