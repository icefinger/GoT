#ifndef __GoTPARAMETERLIST_H_
#define __GoTPARAMETERLIST_H_

#include <GoTParametrizedObject.hh>

namespace icedcode
{
  class GoTParametersList: public GoTParametrizedObject
  {
  public:
    static GoTParametersList* GetIt ();

  private:
    GoTParametersList ();
    static GoTParametersList *fInstance;

  public:
    ObjectParameters* ReadThisConfiguration (libconfig::Setting&) {return 0; /*nothing to do*/}

    virtual GoTParametersMgr* ConstructTheParametersMgr (ObjectParameters* PTP)
    {
      return new GoTParametersMgr (PTP);
    }


  };



}



#endif
