#ifndef __GoTPARAMETRIZEDOBJECT_H_
#define __GoTPARAMETRIZEDOBJECT_H_

#include <G4SystemOfUnits.hh>
#include "G4Types.hh"

#include <map>
#include <string>
#include <regex>

class TFormula;

#include <G4MaterialPropertyVector.hh>

#include <libconfig.h++>

#include <dirent.h>


namespace icedcode
{
  //a sorting tool to avoid map classification troubles, have to replace the longest first
  //but we need to compare through all the maps. Maybe a generalistic map?
  struct classcomp {
    bool operator() (const std::string& lhs, const std::string& rhs) const
    {
      return lhs.size()>rhs.size();
    }
  };

  class GoTParametrizedObject
  {
  protected:
    std::string ConfigFilePrefix = " ";
  protected:
    struct PropertiesStruct {
      std::map <std::string, long> IntegerProperty;
      std::map <std::string, double>                       FloatingProperty;
      std::map <std::string, std::string>                  StringProperty;
      std::map <std::string, std::string>                  FormulaProperty;
      std::map <std::string, std::vector<double> >         ScalarArrayProperty;
      std::map <std::string, std::vector<std::string> >    StringArrayProperty;
    };

    struct ObjectParameters
    {
      std::string Prefix = "";

      PropertiesStruct Properties;

      virtual void CalculateParameters ()
      {
      }

    };

  protected:
    class GoTParametersMgr
    {
    public:
      GoTParametersMgr (ObjectParameters* ptp = NULL) {fTheParameters=ptp;};
      virtual ~GoTParametersMgr ();

      const ObjectParameters* GetParameters () const
      {
        return fTheParameters;
      }

      ObjectParameters        *fTheParameters;
    };


  public:
    virtual void SetParametersListFromMac (std::string Path);
    virtual void SetDataFilesSources (std::string Path);
    virtual void LoadDirectory (std::string);
  protected:
    DIR* ChangeToAbsolutePathAndOpenDirectory (std::string& DIRNAME);
    void ReadAStringParameterFromMac (std::string SPar);
    virtual void ReadConfigurations (std::string);
    virtual ObjectParameters* ReadThisConfiguration (libconfig::Setting&) = 0;
    bool ReadBaseParameters (libconfig::Config*,  ObjectParameters*);
    bool ReadBaseParameters (libconfig::Setting*,  ObjectParameters*);

  public:
    const GoTParametersMgr* GetParametersMgr (std::string);

    long GetIntegerParameter  (const libconfig::Setting& STG, float x=0, float y=0, float z=0);
    double GetFloatingParameter (const libconfig::Setting& STG, float x=0, float y=0, float z=0);
    std::string GetStringParameter   (const libconfig::Setting& STG);
    std::vector<double> GetScalarArrayParameter  (const libconfig::Setting& STG) const;

    long GetIntegerParameter  (std::string NAME);
    double GetFloatingParameter (std::string NAME);
    std::string GetStringParameter   (std::string NAME);
    std::string GetFormulaParameter  (std::string NAME);

  protected:
    GoTParametrizedObject () {
      GoTParametrizedObject::fproject_source_dir = PROJECT_SOURCE_DIR;
    }
    ~GoTParametrizedObject ();

    std::string isAFormula (const libconfig::Setting& STG);
    std::string isAFormula (const std::string& STR);
    void FormulaLooping ();
    std::vector<double> ReadThisArray (const libconfig::Setting& STG) const;
    std::string ReadThisFormula (const libconfig::Setting& STG) const;
    std::string ReadThisFormula (std::string STR) const;

  protected:
    std::map <std::string, ObjectParameters*>      fParametersDict;
    std::map <std::string, GoTParametersMgr*>      fParametersMgrDict;

    static PropertiesStruct fPropertiesStruct;
    std::string fRootName="None";

    /*The parameters' names should start with __*/
    void ReadParameterList (std::string);
    void ReadParameterList (libconfig::Setting &theStg, PropertiesStruct *theProps);
    TFormula* fFormula;


    /*for loop resolver methods*/
    //regex of the for loop
    std::vector <double> ExecuteForFromMatch (const std::string& forCommand);
    bool FindForLoop (const std::string& toInterpret);
    std::vector <double> InterpretWithFor (const std::string& toInterpret);

  protected:
    virtual GoTParametersMgr* ConstructTheParametersMgr (ObjectParameters* PTP) = 0;

  public:
    static std::string fproject_source_dir;
  };
}

#endif
