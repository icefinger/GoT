#!/bin/bash

export GOTROOT=$(readlink -f $(dirname ${BASH_SOURCE[@]})/..)
export GOTLIBPATH=$GOTROOT/build
export GOTINCLUDE=$GOTROOT/include
export GOTLIB=got
export LD_LIBRARY_PATH=$GOTLIBPATH:$LD_LIBRARY_PATH
export LIBRARY_PATH=$GOTLIBPATH:$LIBRARY_PATH
echo $GOTROOT
