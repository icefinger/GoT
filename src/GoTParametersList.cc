#include <GoTParametersList.hh>
#include <string>

using namespace std;

namespace icedcode {

  GoTParametersList* GoTParametersList::fInstance = NULL;

  GoTParametersList* GoTParametersList::GetIt ()
  {
    if (!fInstance)
      fInstance=new GoTParametersList ();
    return (GoTParametersList*)fInstance;
  }

  GoTParametersList::GoTParametersList () {
    ConfigFilePrefix="GoTPar";
    LoadDirectory ((string(PROJECT_SOURCE_DIR)+"/data/"));
  }



}
