#include <TFormula.h>

#include <GoTParametrizedVolume.hh>
#include <sys/types.h>
#include <dirent.h>
#include <iostream>
#include <libconfig.h++>
#include <regex>

#include <G4VisAttributes.hh>


using namespace std;
using namespace libconfig;


namespace icedcode
{

  G4VisAttributes* GoTParametrizedVolume::fgold        = new G4VisAttributes(G4Colour (255./255,185./255,15./255,1));
  G4VisAttributes* GoTParametrizedVolume::fsilver      = new G4VisAttributes(G4Colour (192./255,192./255,192./255,1));
  G4VisAttributes* GoTParametrizedVolume::fbrown      = new G4VisAttributes(G4Colour (102./255,51./255,0./255,1));
  G4VisAttributes* GoTParametrizedVolume::forange      = new G4VisAttributes(G4Colour (255./255,100./255,0./255,1));
  G4VisAttributes* GoTParametrizedVolume::fcyan        = new G4VisAttributes(G4Colour(0.,1.,1.));
  G4VisAttributes* GoTParametrizedVolume::ftransparent = new G4VisAttributes(G4Colour(1.,1.,1.,1.));

  //string GoTParametrizedVolume::ConfigFilePrefix = "          ";

  GoTParametrizedVolume::GoTParametrizedVolume ()
  {
  }

  GoTParametrizedVolume::~GoTParametrizedVolume ()
  {
  }

  G4LogicalVolume* GoTParametrizedVolume::GetLogicalVolume (string NAME)
  {
    if (!fParametersMgrDict.count(NAME))
      {
        if (this->GetParametersMgr (NAME) == NULL)
          return NULL;
      }

    G4LogicalVolume* toreturn = static_cast<GoTVolume*>(fParametersMgrDict[NAME])->Construct ();
    if (!fLogicalVolDict.count(toreturn))
      fLogicalVolDict[toreturn]=NAME;
    return toreturn;
  }

  GoTParametrizedVolume::GoTVolume::~GoTVolume ()
  {
  }

}
