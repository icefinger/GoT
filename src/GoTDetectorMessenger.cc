//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


#include <GoTDetectorMessenger.hh>
#include "GoTDetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "globals.hh"

#include <iostream>

using namespace std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

namespace icedcode
{

  GoTDetectorMessenger::GoTDetectorMessenger(GoTDetectorConstruction* DET) :
    myDetector(DET) {
    GoTDir = new G4UIdirectory("/GoT/");
    GoTDir->SetGuidance("UI commands specific to this example.");

    detDir = new G4UIdirectory("/GoT/det/");
    detDir->SetGuidance("detector control.");

    TargMatCmd = new G4UIcmdWithAString("/GoT/det/setTargetMaterial", this);
    TargMatCmd->SetGuidance("Select Material of the Target.");
    TargMatCmd->SetParameterName("choice", false);
    TargMatCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

    TargLengthCmd = new G4UIcmdWithADoubleAndUnit("/GoT/det/setTargetLength",
                                                  this);
    TargLengthCmd->SetGuidance("Set the Target Length.");
    TargLengthCmd->SetUnitCategory("Length");
    TargLengthCmd->SetParameterName("choice", false);
    TargLengthCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

    WorldLengthCmd = new G4UIcmdWithADoubleAndUnit("/GoT/det/setWorldLength",
                                                   this);
    WorldLengthCmd->SetGuidance("Set the World Length.");
    WorldLengthCmd->SetUnitCategory("Length");
    WorldLengthCmd->SetParameterName("choice", false);
    WorldLengthCmd->AvailableForStates(G4State_PreInit);

    GenerationVolumeLengthCmd = new G4UIcmdWithADoubleAndUnit(
                                                              "/GoT/det/setGenerationVolumeLength", this);
    GenerationVolumeLengthCmd->SetGuidance("Set the Generation Volume Length.");
    GenerationVolumeLengthCmd->SetUnitCategory("Length");
    GenerationVolumeLengthCmd->SetParameterName("choice", false);
    GenerationVolumeLengthCmd->AvailableForStates(G4State_PreInit);

    GenerationVolumeRadiusCmd = new G4UIcmdWithADoubleAndUnit(
                                                              "/GoT/det/setGenerationVolumeRadius", this);
    GenerationVolumeRadiusCmd->SetGuidance("Set the Generation Volume Radius.");
    GenerationVolumeRadiusCmd->SetUnitCategory("Length");
    GenerationVolumeRadiusCmd->SetParameterName("choice", false);
    GenerationVolumeRadiusCmd->AvailableForStates(G4State_PreInit);

    TargetPhi = new G4UIcmdWithADoubleAndUnit(
                                                     "/GoT/det/setTargetPhi", this);
    TargetPhi->SetGuidance("Set the Optical Module Orientation.");
    TargetPhi->SetUnitCategory("Angle");
    TargetPhi->SetParameterName("choice", false);
    TargetPhi->AvailableForStates(G4State_PreInit);

    TargetTheta = new G4UIcmdWithADoubleAndUnit(
                                                       "/GoT/det/setTargetTheta", this);
    TargetTheta->SetGuidance("Set the Optical Module Orientation.");
    TargetTheta->SetUnitCategory("Angle");
    TargetTheta->SetParameterName("choice", false);
    TargetTheta->AvailableForStates(G4State_PreInit);

    TargetPsi = new G4UIcmdWithADoubleAndUnit(
                                                     "/GoT/det/setTargetPsi", this);
    TargetPsi->SetGuidance("Set the Optical Module Orientation.");
    TargetPsi->SetUnitCategory("Angle");
    TargetPsi->SetParameterName("choice", false);
    TargetPsi->AvailableForStates(G4State_PreInit);

    DontDraw = new G4UIcmdWithAString
      ("/GoT/det/DontDraw", this);
    DontDraw->SetGuidance("To avoid to draw some detector elements. Parameters are bento, gel, absorber. Whatever can be a separator (even without it is valid).");
    DontDraw->SetParameterName("choice", false);
    DontDraw->AvailableForStates(G4State_PreInit);

    DetectorTypeName = new G4UIcmdWithAString
      ("/GoT/det/DetectorTypeName", this);
    DetectorTypeName->SetGuidance("new way to create OM dispositions. The name should correspond of one from common/data/GoTDet*.dat.");
    DetectorTypeName->SetParameterName("choice", false);
    DetectorTypeName->AvailableForStates(G4State_PreInit);

    DataFilesSources = new G4UIcmdWithAString
      ("/GoT/det/DataFilesSources", this);
    DataFilesSources->SetGuidance("additionnal folders for data files (as in common/data/GoTDet*.dat). \":\" is the list separator.");
    DataFilesSources->SetParameterName("choice", false);
    DataFilesSources->AvailableForStates(G4State_PreInit);

    ParametersList = new G4UIcmdWithAString
      ("/GoT/det/ParametersList", this);
    ParametersList->SetGuidance("additive parameters for construction, should be in the format \"par1__ = 5;par2__ = 6...");
    ParametersList->SetParameterName("choice", false);
    ParametersList->AvailableForStates(G4State_PreInit);


  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  GoTDetectorMessenger::~GoTDetectorMessenger() {
    delete TargMatCmd;
    delete TargLengthCmd;
    delete GenerationVolumeLengthCmd;
    delete GenerationVolumeRadiusCmd;
    delete TargetTheta;
    delete TargetPhi;
    delete TargetPsi;
    delete detDir;
    delete GoTDir;
    delete DetectorTypeName;
    delete DataFilesSources;
    delete ParametersList;
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


  void GoTDetectorMessenger::SetNewValue(G4UIcommand* command,
                                         G4String newValue) {
    if (command == TargMatCmd) {
      myDetector->SetTargetMaterial(newValue);
    } else if (command == TargLengthCmd) {
      myDetector->SetTargetLength(TargLengthCmd->GetNewDoubleValue(newValue));
    } else if (command == GenerationVolumeLengthCmd) {
      myDetector->SetGenerationVolumeLength
        (GenerationVolumeLengthCmd->GetNewDoubleValue(newValue));
    } else if (command == GenerationVolumeRadiusCmd) {
      myDetector->SetGenerationVolumeRadius
        (GenerationVolumeRadiusCmd->GetNewDoubleValue(newValue));
    } else if (command == WorldLengthCmd) {
      myDetector->SetTargetLength(WorldLengthCmd->GetNewDoubleValue(newValue));
    } else if (command == TargetTheta) {
      myDetector->SetOrientationTheta
        (TargetTheta->GetNewDoubleValue(newValue));
    } else if (command == TargetPhi) {
      myDetector->SetOrientationPhi
        (TargetPhi->GetNewDoubleValue(newValue));
    } else if (command == TargetPsi) {
      myDetector->SetOrientationPsi
        (TargetPsi->GetNewDoubleValue(newValue));
    } else if (command == DontDraw) {
      myDetector->DontDraw
        (newValue);
    } else if (command == DetectorTypeName) {
      myDetector->SetDetectorTypeName (newValue);
    } else if (command == DataFilesSources) {
      myDetector->SetDataFilesSources (newValue);
    } else if (command == ParametersList) {
      myDetector->SetParametersListFromMac (newValue);
    }


  }

}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
