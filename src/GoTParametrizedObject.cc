#include <TFormula.h>

#include <GoTParametrizedVolume.hh>
#include <sys/types.h>
#include <dirent.h>
#include <iostream>
#include <libconfig.h++>
#include <regex>

using namespace std;
using namespace libconfig;


namespace icedcode
{
  string GoTParametrizedObject::fproject_source_dir = PROJECT_SOURCE_DIR;

  GoTParametrizedObject::PropertiesStruct GoTParametrizedObject::fPropertiesStruct;

  GoTParametrizedObject::~GoTParametrizedObject ()
  {
    delete fFormula;
  }

  void GoTParametrizedObject::SetParametersListFromMac (string SParList) {
    size_t doubledotPos=SParList.find (';');
    string subSParList=SParList;
    while (doubledotPos != string::npos) {
      subSParList=SParList.substr (0,doubledotPos);
      cout << "match to search " << subSParList << endl;
      ReadAStringParameterFromMac (subSParList);
      SParList.erase (0,doubledotPos+1);
      doubledotPos=SParList.find(';');
    }
    if (SParList.size ()) {
      ReadAStringParameterFromMac (SParList);
      subSParList=SParList.substr (0,doubledotPos);
    }
  }

  void GoTParametrizedObject::ReadAStringParameterFromMac (std::string SPar) {
    std::cmatch m;
    std::regex_match ( SPar.data (), m, std::regex(" *([a-zA-Z_0-9]+) *= *([0-9]+) *") );
    string Stmp;
    for (unsigned i=1; i<m.size(); ++i)
      {
        if (i%2 == 1){
          std::cout << "match: adding to parameter list: " << m[i];
          Stmp = m[i];
        }else {
          cout << " with value " << stof(m.str(i)) << endl;
          fPropertiesStruct.FloatingProperty[Stmp] = stof(m.str(i));
        }
      }
  }

  void GoTParametrizedObject::SetDataFilesSources (string Path) {
    size_t doubledotPos=Path.find (':');
    string subPath=Path;
    while (doubledotPos != string::npos) {
      subPath=Path.substr (0,doubledotPos);
      LoadDirectory (subPath);
      Path.erase (0,doubledotPos+1);
      doubledotPos=Path.find(':');
    }
    if (Path.size ())
      LoadDirectory (subPath);
  }

  void GoTParametrizedObject::ReadConfigurations (string ConfigFile)
  {
    if (!fFormula)
      fFormula = new TFormula;

    Config cfg;
    cfg.readFile (ConfigFile.data ());
    cfg.setAutoConvert (true);

    if (cfg.exists("Name"))
      fRootName=GetStringParameter(cfg.lookup("Name"));

    Setting& rootStg=cfg.getRoot();

    string Name;

    int configFileSize=rootStg.getLength ();
    for (int cfIt=0; cfIt < configFileSize; cfIt++)
      {
        if (!rootStg[cfIt].isGroup ())
          continue;
        string currentName=rootStg[cfIt].getName ();
        ObjectParameters* objPar=ReadThisConfiguration (rootStg[cfIt]);
        ReadParameterList (rootStg[cfIt], &(objPar->Properties));
      }
    return;
  }

  DIR *GoTParametrizedObject::ChangeToAbsolutePathAndOpenDirectory(string& DIRNAME)
  {
    // try global then local
    DIR* d=opendir(DIRNAME.data ());
    if (d == NULL)
      {
        string localstr;
        localstr = localstr+ fproject_source_dir+"/"+DIRNAME;
        d=opendir(localstr.data ());
        if (d)
          DIRNAME = localstr;
      }
    return d;
  }

  void GoTParametrizedObject::LoadDirectory (string DIRNAME)
  {
    if (!fFormula)
      fFormula=new TFormula;


    struct dirent *de=NULL;
    DIR *d=NULL;

    d=ChangeToAbsolutePathAndOpenDirectory(DIRNAME);
    if(d == NULL)
      {
        string localstr;
        localstr = localstr+fproject_source_dir+"/"+DIRNAME;
        cerr << "Couldn't open directory " << DIRNAME << " nor " << localstr << endl;
        return;
      }

    // Loop while not NULL
    while( (de = readdir(d)) )
      {
        string filename=de->d_name;
        if (de->d_type == 8 && filename.substr (0,ConfigFilePrefix.size ()) == ConfigFilePrefix &&
            filename.rfind (".dat") == filename.size () - 4)
          {
            clog << "GoTParametrizedObject: Loading file " << DIRNAME  << '/' << de->d_name<< endl;
            ReadParameterList (DIRNAME+"/"+filename);
            ReadConfigurations (DIRNAME+"/"+filename);
          }
      }
    closedir(d);
  }

  bool GoTParametrizedObject::ReadBaseParameters (Config* cfg, ObjectParameters* thePTP)
  {
    return ReadBaseParameters (&cfg->getRoot (), thePTP);
  }

  bool GoTParametrizedObject::ReadBaseParameters (Setting* stg, ObjectParameters* thePTP)
  {

    //stg->lookupValue("CheckOverlaps"      , thePTP->CheckOverlaps); //!!!warning
    return
      stg->lookupValue("Prefix"       , thePTP->Prefix);
  }

  vector <double> GoTParametrizedObject::ReadThisArray (const libconfig::Setting& STG) const
  {
    vector <double> tmpVect;
    if  (STG.getType () != Setting::TypeArray)
      {
        cerr << "ERROR: GoTParametrizedObject::ReadThisArray: I'm asked to read a setting which is not an array, the SettingType is " << STG.getType () << ", I'll do nothing."<< endl;
        return tmpVect;
      }
    for (int index=0; index<STG.getLength (); index++)
      tmpVect.push_back (STG[index]);

    return tmpVect;
  }

  void GoTParametrizedObject::ReadParameterList (Setting &theStg, PropertiesStruct *theProps)
  {
    string ParSuffix="__";
    int SettingSize=theStg.getLength ();
    for (int it=0; it<SettingSize; it++)
      {
        Setting& CurrentSetting=theStg[it];
        string SettingName=CurrentSetting.getName ();
        int strSize=SettingName.size ();
        if (strSize > 2 && SettingName.substr(strSize-2,strSize-1) == ParSuffix)
          {
            if (CurrentSetting.getType () == Setting::TypeInt ||
                CurrentSetting.getType () == Setting::TypeInt64)
              theProps->IntegerProperty[SettingName] = long(CurrentSetting);
            else if (CurrentSetting.getType () == Setting::TypeFloat)
              theProps->FloatingProperty[SettingName] = double(CurrentSetting);
            else if (CurrentSetting.getType () == Setting::TypeString)
              {
                if (FindForLoop((const char*)CurrentSetting))
                  {
                    theProps->ScalarArrayProperty[SettingName]=InterpretWithFor ((const char*)CurrentSetting);
                    return;
                  }
                string SubFormula = isAFormula (CurrentSetting);
                if (SubFormula.size ())
                  theProps->FormulaProperty[SettingName]=SubFormula;
                else
                  theProps->StringProperty[SettingName] = (const char*)CurrentSetting;
              }
            else if (CurrentSetting.getType () == Setting::TypeArray)
              {
                theProps->ScalarArrayProperty[SettingName]=ReadThisArray (CurrentSetting);
              }

          }
      }
  }

  void GoTParametrizedObject::ReadParameterList (std::string FILENAME)
  {
    Config cfg;
    cfg.setAutoConvert (true);
    cfg.readFile (FILENAME.data ());

    Setting& stgRoot= cfg.getRoot ();

    ReadParameterList (stgRoot, &fPropertiesStruct);

  }

  void GoTParametrizedObject::FormulaLooping ()
  {
    for (auto& aStrPair: fPropertiesStruct.StringProperty)
      {
        string SubFormula = isAFormula (aStrPair.second);
        if (SubFormula.size ())
          {
            fPropertiesStruct.FormulaProperty[aStrPair.first]=SubFormula;
            fPropertiesStruct.StringProperty.erase (aStrPair.first);
          }
      }
  }

  string GoTParametrizedObject::isAFormula (const Setting& STG)
  {
    string aStr = (const char*)STG;
    return isAFormula (aStr);
  }

  string GoTParametrizedObject::isAFormula (const string& STR)
  {
    string sformula = ReadThisFormula (STR);
    int isOk = fFormula->Compile (sformula.data ());
    if (isOk == 0)
      return sformula;
    return "";

  }
  string GoTParametrizedObject::ReadThisFormula (const Setting& STG) const
  {
    string strToTest = (const char*)STG;
    return ReadThisFormula (strToTest);
  }

  string GoTParametrizedObject::ReadThisFormula (string strToTest) const
  {
  beginWhileTest:
    if (strToTest.find ("__") != string::npos)
      {
        for (const auto& parPair: fPropertiesStruct.FloatingProperty)
          {
            size_t posToReplace=0;
            posToReplace = strToTest.find (parPair.first);
            if (posToReplace == string::npos)
              continue;
            string substitute = to_string (parPair.second);
            strToTest.replace (posToReplace, parPair.first.size (), substitute);
            goto beginWhileTest;
          }
        for (const auto& parPair: fPropertiesStruct.IntegerProperty)
          {
            size_t posToReplace=0;
            posToReplace = strToTest.find (parPair.first);
            if (posToReplace == string::npos)
              continue;
            string substitute = to_string (parPair.second);
            strToTest.replace (posToReplace, parPair.first.size (), substitute);
            goto beginWhileTest;
          }
        for (const auto& parPair: fPropertiesStruct.FormulaProperty)
          {
            size_t posToReplace=0;
            posToReplace = strToTest.find (parPair.first);
            if (posToReplace == string::npos)
              continue;
            string substitute = parPair.second;
            strToTest.replace (posToReplace, parPair.first.size (), substitute);
            goto beginWhileTest;
          }

      }
    return strToTest;
  }

  vector<double> GoTParametrizedObject::GetScalarArrayParameter (const Setting& STG) const
  {
    if (STG.getType () == Setting::TypeArray)
      return ReadThisArray(STG);

    if (STG.getType () != Setting::TypeString)
      {
        //cerr << "WARNING:" << STG.getName () << " parameter is not and int neither a string, return empty vector."<<endl;
        return vector<double>();
      }

    string Name = (const char*) STG;

    if (fPropertiesStruct.ScalarArrayProperty.count(Name))
      {
        return fPropertiesStruct.ScalarArrayProperty.at(Name);
      }

    cerr << "WARNING:" << STG.getName () << " does not exist in table, might not be inizializated, or the parameter name does not end with \"__\"" << endl;
    return vector<double>();
  }

  long GoTParametrizedObject::GetIntegerParameter (const Setting& STG, float x, float y, float z)
  {
    if (STG.getType () == Setting::TypeInt ||
        STG.getType () == Setting::TypeInt64)
      return STG;
    if (STG.getType () != Setting::TypeString)
      {
        cerr << "WARNING:" << STG.getName () << " parameter is not and int neither a string, return max of double."<<endl;
        return 0xffffffffffffffff;
      }

    string Name = (const char*) STG;

    if (fPropertiesStruct.IntegerProperty.count(Name))
      {
        return fPropertiesStruct.IntegerProperty[Name];
      }

    if (fPropertiesStruct.FormulaProperty.count(Name))
      {
        fPropertiesStruct.FormulaProperty[STG.getName ()]=ReadThisFormula (STG);
        fFormula->Compile (fPropertiesStruct.FormulaProperty[STG.getName ()].data ());
        return fFormula->Eval (x,y,z);
      }

    cerr << "WARNING:" << STG.getName () << " does not exist in table, might not be inizializated, or the parameter name does not end with \"__\"" << endl;
    return 0xffffffffffffffff;
  }

  double GoTParametrizedObject::GetFloatingParameter (const Setting& STG, float x, float y, float z)
  {
    if (STG.getType () == Setting::TypeFloat ||
        STG.getType () == Setting::TypeInt ||
        STG.getType () == Setting::TypeInt64)
      return STG;
    if (STG.getType () != Setting::TypeString)
      {
        cerr << "WARNING:" << STG.getName () << " parameter is not and double/float neither a string, return max of double."<<endl;
        return 0xffffffffffffffff;
      }

    string Name = (const char*) STG;

    if (fPropertiesStruct.FloatingProperty.count(Name))
      {
        return fPropertiesStruct.FloatingProperty[Name];
      }

    if (fPropertiesStruct.IntegerProperty.count(Name))
      {
        return fPropertiesStruct.IntegerProperty[Name];
      }

    if (fPropertiesStruct.FormulaProperty.count(Name))
      {
        fFormula->Compile (fPropertiesStruct.FormulaProperty[Name].data ());
        return fFormula->Eval (x,y,z);
      }

    if (STG.getType () == Setting::TypeString)
      {
        string SubFormula = isAFormula (STG);
        if (SubFormula.size ())
          {
            fPropertiesStruct.FormulaProperty[STG.getName ()]=ReadThisFormula (STG);
            fFormula->Compile (fPropertiesStruct.FormulaProperty[STG.getName ()].data ());
            return fFormula->Eval (x,y,z);
          }
      }


    cerr << "WARNING:" << Name << " does not exist in table, might not be inizializated, or the parameter name does not end with \"__\"" << endl;
    return 0xffffffffffffffff;
  }

  string GoTParametrizedObject::GetStringParameter (const Setting& STG)
  {
    if (STG.getType () != Setting::TypeString)
      {
        cerr << "WARNING:" << STG.getName () << " parameter is not a string."<<endl;
        return "";
      }

    string SValue = (const char*)STG;

    if (!fPropertiesStruct.StringProperty.count(SValue))
      {
        return SValue;
      }
    return fPropertiesStruct.StringProperty[SValue];
  }

  long GoTParametrizedObject::GetIntegerParameter (string NAME)
  {
    if (fPropertiesStruct.IntegerProperty.count(NAME))
      return fPropertiesStruct.IntegerProperty[NAME];
    return 0xffffffffffffffff;
  }

  double GoTParametrizedObject::GetFloatingParameter (string NAME)
  {
    if (fPropertiesStruct.FloatingProperty.count(NAME))
      return fPropertiesStruct.FloatingProperty[NAME];
    return 0xffffffffffffffff;
  }

  string GoTParametrizedObject::GetFormulaParameter (string NAME)
  {
    if (fPropertiesStruct.FormulaProperty.count(NAME))
      return fPropertiesStruct.FormulaProperty[NAME];
    return "";
  }

  string GoTParametrizedObject::GetStringParameter (string NAME)
  {
    if (fPropertiesStruct.StringProperty.count(NAME))
      return fPropertiesStruct.StringProperty[NAME];
    return "";
  }

  const GoTParametrizedObject::GoTParametersMgr* GoTParametrizedObject::GetParametersMgr (string NAME)
  {
    if (!fParametersDict.count (NAME))
      {
        cerr << "GoTParametrizedObject::GoTParametersMgr warning, cannot create " << NAME <<", it does not exist in the dictionary. return NULL."<< endl;
        return NULL;
      }
    if (!fParametersMgrDict.count (NAME))
      {
        fParametersMgrDict[NAME] = ConstructTheParametersMgr (fParametersDict[NAME]);
      }
    return fParametersMgrDict[NAME];
  }

  GoTParametrizedObject::GoTParametersMgr::~GoTParametersMgr ()
  {
  }
  /*for loop stuffs*/
  const std::regex fForRegex(" *for *([a-zA-Z0-9]+__) *in *\\( *([0-9]+) *; *([0-9]+) *\\)(.*)");
  std::vector <double> GoTParametrizedObject::ExecuteForFromMatch (const string& forCommand)
  {
    vector <double> toReturn;
    std::smatch sm;
    regex_match (forCommand, sm, fForRegex );

    string varName=sm[1];
    int start=stoi(sm[2]);
    int stop= stoi(sm[3]);

    for (int current=start;current!=stop; current++)
      {
        fPropertiesStruct.FloatingProperty[varName]=current;
        vector <double> currentExectution=InterpretWithFor (sm[4]);
        toReturn.insert(toReturn.end(), currentExectution.begin (), currentExectution.end ());
      }

    return toReturn;
  }

  bool GoTParametrizedObject::FindForLoop (const string& toInterpret)
  {
    std::smatch m;
    return regex_search (toInterpret,m,fForRegex);
  }

  vector <double> GoTParametrizedObject::InterpretWithFor (const string& toInterpret)
  {
    vector <double> toReturn;
    if (FindForLoop(toInterpret))
      {
        vector <double> currentExectution=ExecuteForFromMatch (toInterpret);
        toReturn.insert(toReturn.end(), currentExectution.begin (), currentExectution.end ());
      }
    else
      {
        string SubFormula = isAFormula (toInterpret);
        if (SubFormula.size ())
          {
            fFormula->Compile (SubFormula.data ());
            fFormula->Eval (0,0,0);
          }
        else
          cerr << "For loop error, cannot interpret the formula " << SubFormula << endl;
      }
    return toReturn;
  }

}
