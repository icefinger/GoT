//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


#include <GoTExperimentMessenger.hh>
#include <GoTDetectorConstruction.hh>

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "globals.hh"

#include <iostream>

using namespace std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

namespace icedcode
{
  GoTExperimentMessenger* GoTExperimentMessenger::fInstance=0;
  GoTExperimentMessenger* GoTExperimentMessenger::GetIt () {
    if (fInstance == 0)
      fInstance = new GoTExperimentMessenger;
    return fInstance;
  }

  GoTExperimentMessenger::GoTExperimentMessenger()
 {
    fDetector = NULL;

    GoTDir = new G4UIdirectory("/GoT/");
    GoTDir->SetGuidance("UI commands specific to this example.");

    XPDir = new G4UIdirectory("/GoT/XP/");
    XPDir->SetGuidance("detector control.");

    XPCommand = new G4UIcmdWithAString("/GoT/XP/Name", this);
    XPCommand->SetGuidance("Select The type of XP, AA K40 or WProp.\nAA can contain Antares(default), NEMO or GoTNeT");
    XPCommand->SetParameterName("choice", false);
    XPCommand->AvailableForStates(G4State_PreInit);
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  GoTExperimentMessenger::~GoTExperimentMessenger() {
    delete XPCommand;
    delete GoTDir;
  }

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

  void GoTExperimentMessenger::SetNewValue(G4UIcommand* command,
                                               G4String newValue) {
    if (command == XPCommand) {
      cout << newValue << " <- detector name " << fDetector <<endl;
      if (!frunManager)
        {
          throw ("ERROR:GoTExperimentMessenger needs a G4RunManager!!\n") ;
        }
      if (fDetector == NULL)
        {
          fDetector = GoTDetectorConstruction::GetIt ();
        } else {
        throw("XP is already initiated!! The command /GoT/XP/Type should be done at the very beginning and only once!!\n");
      }
    }
  }

}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
